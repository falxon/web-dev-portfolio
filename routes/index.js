//import  {authen}  from 'key.js';

var express = require('express');
const app = express();
var router = express.Router();

const fs = require('fs');

var nodemailer = require('nodemailer');



let dat = fs.readFileSync("routes/key.json");
let authen = JSON.parse(dat);

var transporter = nodemailer.createTransport(authen);

let contactResContent = require('../site_content/contactRes.js');

let englishLinks = [
  {
    path: "en",
    name: "Home"
  },
  {
    path: "en/about",
    name: "About"
  },
  {
    path: "en/contact",
    name: "Contact"
  }
];

let spanishLinks = [
  {
    path: "es",
    name: "Inicio"
  },
  {
    path: "es/acerca-de",
    name: "Acerca De"
  },
  {
    path: "es/contacto",
    name: "Contacto"
  }
];




// app.use(express.urlencoded({
//   extended: true
// }))]
//const app = express();
router.use(require('body-parser').urlencoded({ extended: false }));

router.post('/contact',function(req,res){
  req.body; // { answer: 42 }
  //res.json(req.body);

  let mailOptions = {
    from: 'emberashdown@gmail.com',
    replyTo:req.body['contact-email'],
    to: 'embersundown@protonmail.com',
    subject: req.body['contact-subject'],
    text: req.body.message
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      res.render('index',
      {
        title: 'Contact Me',
        banner: {
          text: "There has been an error with your message. Please try again later"
        },
        mixs: [
          {
            type: "form",
            inputs: [
              {
                name: "contact-name",
                label: "Name",
                placeholder: "Your Name Here",
                type: "text"
              },
              {
                name: "contact-email",
                label: "Email",
                placeholder: "Your email here",
                type: "email"
              },
              {
                name: "contact-subject",
                label: "Subject",
                placeholder: "eg. Website Enquiry",
                type: "text"
              },
              {
                name: "message",
                label: "Message",
                placeholder: "Please type your message here",
                type: "textarea"
              },
              {
                name:"contact-submit",
                label:"Submit",
                placeholder:"",
                type:"submit"
              }
            ]
          }
        ],
        features: [
          {
            title: "Contact Me",
            body: "For web development enquiries, please contact me via my contact form here"
          },
          {
            title: "Another Featured thing",
            body: "the text of another featured thing sdiosdoihso dohsiodhs sdiohoihos sdoihsdoihdois sdoihsodhsodih oisdhoisdh"
          },
          {
            title: "And another",
            body: "because things come in 3s"
          }
        ]
      });
    } else {
      res.render('index', contactResContent);
    }
  });

  //res.end();
})

/* GET home page. */
router.get('/', function(req, res, next) {
  let homeContent = require('../site_content/start.js');
  //homeContent.header = englishLinks;
  res.render('index', homeContent);
});
router.get('/en', function(req, res, next) {
  let homeContent = require('../site_content/english/home.js');
  homeContent.header = englishLinks;
  res.render('index', homeContent);
});
router.get('/es', function(req, res, next) {
  let homeContent = require('../site_content/home.js');
  homeContent.header = spanishLinks;
  res.render('index', homeContent);
});

router.get('/en/about', function(req, res, next) {
  let aboutContent = require('../site_content/about.js');
  aboutContent.header = englishLinks;
  res.render('index', aboutContent);
});
router.get('/es/acerca-de', function(req, res, next) {
  let aboutContent = require('../site_content/about.js');
  aboutContent.header = spanishLinks;
  res.render('index', aboutContent);
});

router.get('/en/contact', function(req, res, next) {
  let contactContent = require('../site_content/contact.js');
  contactContent.header = englishLinks;
  res.render('index', contactContent);
});
router.get('/es/contacto', function(req, res, next) {
  let contactContent = require('../site_content/contact.js');
  contactContent.header = spanishLinks;
  res.render('index', contactContent);
});


module.exports = router;
