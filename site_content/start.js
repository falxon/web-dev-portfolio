module.exports = {
  title: 'Welcome to website',
  mixs: [
    {
      type: "para",
      col: "one",
      para: ["This website is available in English and Spanish. Please use the buttons below to navigate to the site of your choice"]
    },
    {
      type: "para",
      col: "two",
      para: ["Este sitio de web está disponible en español o inglés. Por favor utiliza los botones por debajo"]
    }
  ]
}
