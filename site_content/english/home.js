module.exports = { title: 'Web Dev Business',
headPage: true,
headPageContent: {
  title: "Web Development Torrevieja",
  left: {
    title: "Do you need a website for your business or personal needs?",
    checkList: ["Fast, modern websites","Responsive design for mobile","Built in JavaScript using express server","E-commerce available","User-friendly interface for site editing","Affordable prices","Bilingual or multilingual sites","Translation to English available"]
  },
  right: {
    content: "for information on options, prices, or to discuss your website",
    button: "Contact me now",
    link: "/en/contact"
  },
  bottomMessage: "scroll down for more"
},
mixs: [
  {
    type: "img",
    img: "images/code.png",
    para: ["Websites are the ultimate online presence, allowing you to connect with your audience without the restrictions imposed by social media. Whatever your business or personal needs are, I can work with you to develop the perfect site for you. This includes:","<ul><li>Business page</li><li>Webshop</li><li>Personal Blog</li><li>Portfolio</li><li>And many more</li></ul>","All my sites come with some basic and advanced features such as search engine optimisation (SEO), responsive design allowing for ease of use on mobile devices, TLS(SSL) certificate for safer browsing, social media integration and easy editability by the customer. Email with a custom domain, translation and bespoke sites and solutions are available at added cost."]
  },
  {
    type: "para",
    col: "both",
    para: ["Cupiditate rerum eum saepe culpa est dolores alias dolor. Sunt sed omnis provident voluptas sit. Ut excepturi accusantium facilis. Eligendi dolor autem aperiam voluptas. Eos quia placeat sint dolores ad non quod.","Autem libero doloribus fugit qui at reprehenderit. Recusandae nihil iusto amet quas enim omnis eum. Odit aperiam pariatur eius totam nihil et. Aut aut corporis minima mollitia fugit quidem eligendi.","Cupiditate rerum eum saepe culpa est dolores alias dolor. Sunt sed omnis provident voluptas sit. Ut excepturi accusantium facilis. Eligendi dolor autem aperiam voluptas. Eos quia placeat sint dolores ad non quod.","Autem libero doloribus fugit qui at reprehenderit. Recusandae nihil iusto amet quas enim omnis eum. Odit aperiam pariatur eius totam nihil et. Aut aut corporis minima mollitia fugit quidem eligendi.","Cupiditate rerum eum saepe culpa est dolores alias dolor. Sunt sed omnis provident voluptas sit. Ut excepturi accusantium facilis. Eligendi dolor autem aperiam voluptas. Eos quia placeat sint dolores ad non quod.","Autem libero doloribus fugit qui at reprehenderit. Recusandae nihil iusto amet quas enim omnis eum. Odit aperiam pariatur eius totam nihil et. Aut aut corporis minima mollitia fugit quidem eligendi."]
  }
],
features: [
  {
    title: "Contact Me",
    body: "For web development enquiries, please contact me via my contact form here"
  },
  {
    title: "Another Featured thing",
    body: "the text of another featured thing sdiosdoihso dohsiodhs sdiohoihos sdoihsdoihdois sdoihsodhsodih oisdhoisdh"
  },
  {
    title: "And another",
    body: "because things come in 3s"
  }
]
}
