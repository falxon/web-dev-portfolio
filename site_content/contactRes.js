module.exports = {
  title: 'Contact Me',
  banner: {
    text: "Your message has been sent"
  },
  mixs: [
    {
      type: "form",
      inputs: [
        {
          name: "contact-name",
          label: "Name",
          placeholder: "Your Name Here",
          type: "text"
        },
        {
          name: "contact-email",
          label: "Email",
          placeholder: "Your email here",
          type: "email"
        },
        {
          name: "contact-subject",
          label: "Subject",
          placeholder: "eg. Website Enquiry",
          type: "text"
        },
        {
          name: "message",
          label: "Message",
          placeholder: "Please type your message here",
          type: "textarea"
        },
        {
          name:"contact-submit",
          label:"Submit",
          placeholder:"",
          type:"submit"
        }
      ]
    }
  ],
  features: [
    {
      title: "Contact Me",
      body: "For web development enquiries, please contact me via my contact form here"
    },
    {
      title: "Another Featured thing",
      body: "the text of another featured thing sdiosdoihso dohsiodhs sdiohoihos sdoihsdoihdois sdoihsodhsodih oisdhoisdh"
    },
    {
      title: "And another",
      body: "because things come in 3s"
    }
  ]
}
